# Demo booking

Demo of basic form using HTML, CSS, and JavaScript.

Use case: booking depending on people number.

Functional requirements:
- input contact (name, phone), booking (type, date), and referral (social network)
- cancel while booking
- feedback on result

Non-functional requirements:
- intuitive
- responsive

Annotations:
- requirements analysis is limited to form frontend, analyzing  backend will lead to more complex considerations.
- it requires internet access as it loads Materialize CSS minified files from CDN.
- it simulates requesting a server during one second.
- it doesn't validate any input (e.g. it's possible to select a date before today)

![](demo.gif)

References:
- https://materializecss.com/
