var FORM_BOOKING = '#booking'
var MODAL_BOOKING = '#modal-booking-loading'

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector(FORM_BOOKING).addEventListener('submit', handleSubmit);

  var options = {
    deafultDate: Date()
  };
  var elems = document.querySelectorAll('.datepicker');
  var instances = M.Datepicker.init(elems, options);
  console.log(instances);

  var elems = document.querySelectorAll(MODAL_BOOKING);
  var instances = M.Modal.init(elems);
  console.log(instances);

  var elems = document.querySelectorAll('select');
  var instances = M.FormSelect.init(elems, options);
});

function fetchSubmit(onSuccess, onFailure) {
  /* simulate requesting server */
  new Promise((resolve, _) => setTimeout(resolve, 1000))
    .then(onSuccess)
    .catch(onFailure);
}

function handleSubmit(event) {
  event.preventDefault();

  var elem = document.querySelector(MODAL_BOOKING);
  var instance = M.Modal.getInstance(elem);

  instance.open();
  fetchSubmit(
    () => {
      instance.close();
      M.toast({html: 'Correctly booked!'})
      document.querySelector(FORM_BOOKING).reset()
    },
    () => {
      instance.close();
      M.toad({html: "Booking failed :("})
      document.querySelector(FORM_BOOKING).reset()
    },
  );
}
